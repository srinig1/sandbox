/**
 * 
 */
package com.itascadata.customcalculations;

import java.security.InvalidParameterException;

/**
 * This class calculates various Financial Ratios 
 * @author Srini Govindan
 * @version 1.0
 *
 */
public final class FinancialRatios {

	/**
	 * Returns Quick Ratio given valid cash, marketable Securities, receivable and current Liabilities
	 * @param cash Cash
	 * @param marketableSecurities Marketable Securities
	 * @param receivables Receivables
	 * @param currentLiabilities Current Liabilities
	 * @return Quick Ratio
	 * @throws InvalidParameterException
	 */
	public static double quickRatio(double cash, double marketableSecurities, double receivables, double currentLiabilities) /*throws InvalidParameterException*/ {
		
		if(cash < 0) throw new InvalidParameterException("Invalid Cash : " + cash);
		if(marketableSecurities < 0) throw new InvalidParameterException("Invalid Marketable Securities : " + marketableSecurities);
		if(receivables < 0) throw new InvalidParameterException("Invalid Receivables : " + receivables);
		if(currentLiabilities <= 0) throw new InvalidParameterException("Invalid Current Liabilities : " + currentLiabilities);
	
		return (cash + marketableSecurities + receivables)/currentLiabilities;
	}

}


