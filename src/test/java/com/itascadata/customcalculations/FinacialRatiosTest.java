/**
 * 
 */
package com.itascadata.customcalculations;

import static org.junit.Assert.*;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.security.InvalidParameterException;

/**
 * This class tests various methods is FinancialRatios class 
 * @author Srini Govindan
 * @version 1.0
 *
 */
public class FinacialRatiosTest {

	@Rule
    public ExpectedException exception= ExpectedException.none();
	
	@Test
	public void quickRatioTestWithValidValues() {
		assertEquals("Should return Quick Ratio as 6 if cash = 100, marketable securities = 200, receivables = 300 and current liabilities = 100", 
					  6.0, 
					  FinancialRatios.quickRatio(100, 200, 300, 100), 0);
	}

	@Test
	public void quickRatioTestWithInvalidCash() {
		exception.expect(InvalidParameterException.class);
		exception.expectMessage("Invalid Cash : -100");
		FinancialRatios.quickRatio(-100, 200, 300, 100);
	}
	
	@Test
	public void quickRatioTestWithInvalidMarketableSecurities() {
		exception.expect(InvalidParameterException.class);
		exception.expectMessage("Invalid Marketable Securities : -200");
		FinancialRatios.quickRatio(100, -200, 300, 100);
	}
	
	@Test
	public void quickRatioTestWithInvalidReceivables() {
		exception.expect(InvalidParameterException.class);
		exception.expectMessage("Invalid Receivables : -300");
		FinancialRatios.quickRatio(100, 200, -300, 100);
	}
	
	@Test
	public void quickRatioTestWithNegativeCurrentLiabilities() {
		exception.expect(InvalidParameterException.class);
		exception.expectMessage("Invalid Current Liabilities : -100");
		FinancialRatios.quickRatio(100, 200, 300, -100);
	}
	
	@Test
	public void quickRatioTestWithCurrentLiabilitiesAsZero() {
		exception.expect(InvalidParameterException.class);
		exception.expectMessage("Invalid Current Liabilities : 0");
		FinancialRatios.quickRatio(100, 200, 300, 0);
	}
}
